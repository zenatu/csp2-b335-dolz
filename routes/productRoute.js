const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.post("/", verify, verifyAdmin, productController.addProduct);
router.get("/", verify, productController.getAllActiveProducts);
router.get("/all", verify, verifyAdmin, productController.getAllProducts);
// Specific Product
router.get("/:productId", productController.getProduct);
// Update/Archive and Activate Product
router.patch("/:productId/update", verify, verifyAdmin, productController.updateProduct);
router.patch("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);
router.patch("/:productId/activate", verify, verifyAdmin, productController.activateProduct);
// Search products by name
router.post("/searchByName", productController.searchProductsByName);
// Search products by price range
router.post("/searchByPrice", productController.searchProductsByPriceRange);
// Search Products
router.post("/search", productController.searchProducts);






module.exports = router;

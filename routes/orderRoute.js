const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const cartController = require("../controllers/cartController");
const orderController = require("../controllers/orderController");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;


// ---------- USER Checkout from Cart to Order
// Route for non-admin user checkout
router.post("/checkout", verify, cartController.checkout);

// Route to get all orders (admin only)
router.get('/all-orders', verify, verifyAdmin, orderController.getAllOrders);
router.get('/my-orders', verify, orderController.getUserOrders);

module.exports = router;
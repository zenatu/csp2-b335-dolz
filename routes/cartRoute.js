const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;



// ------------------ CART

router.get("/get-cart", verify, cartController.getUserCart);
router.post("/add-to-cart", verify, cartController.addToCart);
router.patch("/update-cart-quantity", verify, cartController.changeQuantity)
router.delete("/:productId/remove-from-cart", verify, cartController.removeProductFromCart);
router.delete("/clear-cart", verify, cartController.clearCart);


// ---------- USER Checkout from Cart to Order


// Route for non-admin user checkout
// router.post("/checkout", verify, cartController.checkout);




module.exports = router;
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
// const cartController = require("../controllers/cartController");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;


router.post("/", userController.registerUser);
router.post("/login", userController.loginUser);
router.get("/details", verify, userController.getDetails);
router.patch("/:userId/set-as-admin", verify, verifyAdmin, userController.updateToAdmin);
// router.patch("/:userId/demote-from-admin", verify, verifyAdmin, userController.demoteFromAdmin);
router.post("/update-password", verify, userController.updatePassword);

// ------------------ CART

// router.get("/get-cart", verify, cartController.getUserCart);
// router.post("/cart/add-to-cart", verify, cartController.addToCart);
// router.patch("/cart/update-cart-quantity", verify, cartController.changeQuantity)
// router.delete("/cart/:productId/remove-from-cart", verify, cartController.removeProductFromCart);
// router.delete("/cart/clear-cart", verify, cartController.clearCart);


// ---------- USER Checkout from Cart to Order


// Route for non-admin user checkout
// router.post("/checkout", verify, cartController.checkout);




module.exports = router;
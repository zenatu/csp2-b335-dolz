// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

// Routes
const userRoutes = require("./routes/userRoute");
const productRoutes = require("./routes/productRoute");
const orderRoutes = require("./routes/orderRoute");
const cartRoutes = require("./routes/cartRoute");

const port = 4004;
const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())

// Connection to MongoDB
mongoose.connect("mongodb+srv://admin:admin@b335-dolz.jqjbulr.mongodb.net/SanaMall",
	{
			useNewUrlParser : true,
			useUnifiedTopology : true
	})

mongoose.connection.once('open', () => console.log('Now connected to Mongodb Atlas'));

// URI for Routes
app.get("/b4", (req, res) => {
	res.send("Hello world")
})
app.use("/b4/users", userRoutes);
app.use("/b4/products", productRoutes);
app.use("/b4/orders", orderRoutes);
app.use("/b4/cart", cartRoutes);

app.listen(process.env.PORT || port, () => {console.log(`API is now onlinoe on port ${process.env.PORT || port}`)})
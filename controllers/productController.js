const Product = require ("../models/Product");


// Adds a product
module.exports.addProduct = (req, res) => {

	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	});

	Product.findOne({ name: req.body.name })
	.then(existingProduct => {
	    if (existingProduct) {
	        return res.status(409).send({ error: 'Product already exists' });
	    }

	    return newProduct.save()
	    .then(savedProduct => res.status(201).send({ savedProduct }))
	    .catch(saveErr => {
	        console.error("Error in saving the product: ", saveErr)
	        return res.status(500).send({ error: 'Failed to save the product' });
	    }); 
	})
	.catch(findErr => {
	    console.error("Error in finding the product: ", findErr)
	    return res.status(500).send({ message:'Error finding the product' });
	});

};

// Retrieves all Products
module.exports.getAllActiveProducts = (req, res) => {
	return Product.find({isActive : true})
	.then(products => {
	    if(products.length > 0){
	    	res.status(200).send({ products });
	    } else {
	        return res.status(200).send({ message: 'No products found.' });
	    }
	}).catch (err => res.status(500).send({ error: 'Error finding products.'}))
};


module.exports.getAllProducts = (req, res) => {
    return Product.find({})
    .then(products => {
        if(products.length > 0){
            res.status(200).send({ products });
        } else {
            return res.status(200).send({ message: 'No products found.' });
        }
    }).catch (err => res.status(500).send({ error: 'Error finding products.'}))
};



module.exports.getProduct = (req, res) => {
	console.log(req.params.productId)
    Product.findById(req.params.productId)
    .then(product => {
    	console.log(product)
        if (!product) {
            return res.status(404).send({ error: 'Product not found' });
        }
        return res.status(200).send({ product });
    }).catch(err => {
     console.error("Error in fetching the product: ", err)
     return res.status(500).send({ error: 'Failed to fetch product' });
    }); 
};


module.exports.updateProduct = (req, res) =>{
    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then(result => {
        if(!result){
            return res.status(404).send({error: 'Product not found'});
        } else {
            return res.status(200).send({
                message: 'Product updated successfully',
                updatedProduct: updatedProduct
            })
        }
    }).catch(err => {
        console.error("Error in updating a product: ", err)
        return res.status(500).send({ error: 'Error in updating a product.' });
    });
}



module.exports.archiveProduct = (req, res) => {
    let updateActiveField = {
        isActive: false
    }
    if (req.user.isAdmin == true){
        return Product.findByIdAndUpdate(req.params.productId, updateActiveField, {new:true})
        .then(archiveProduct => {
            if (!archiveProduct) {
            	return res.status(404).send({ error: 'Product not found' });
            } 
            return res.status(200).send({ 
            	message: 'Product archived successfully', 
            	archiveProduct: archiveProduct 
            });
        })
        .catch(err => {
        console.error("Error in archiving a product: ", err)
        return res.status(500).send({ error: 'Failed to archive product' })
        });
    } else {
        return res.status(403).send(false);
    }
};


module.exports.activateProduct = (req, res) => {

    let updateActiveField = {
        isActive: true
    }
    if (req.user.isAdmin == true){
        return Product.findByIdAndUpdate(req.params.productId, updateActiveField, {new:true})
        .then(activateProduct => {
            if (!activateProduct) {
            	return res.status(404).send({ error: 'Product not found' });
            } 
            return res.status(200).send({ 
            	message: 'Product activated successfully', 
            	activateProduct: activateProduct 
            });
        })
        .catch(err => {
        console.error("Error in activating a product: ", err)
        return res.status(500).send({ error: 'Failed to activate product' })
        });
    } else {
        return res.status(403).send(false);
    }
};


module.exports.searchProductsByName = async (req, res) => {
  try {
    const { name } = req.body;

    if (!name) {
      return res.status(400).json({ message: 'Name parameter is required' });
    }

    const products = await Product.find({ name: { $regex: new RegExp(name, 'i') } });

    res.status(200).json({ products });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

module.exports.searchProductsByPriceRange = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    if (!minPrice || !maxPrice) {
      return res.status(400).json({ message: 'Both minPrice and maxPrice parameters are required' });
    }

    const products = await Product.find({ price: { $gte: minPrice, $lte: maxPrice } });

    res.status(200).json({ products });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};




module.exports.searchProducts = async (req, res) => {
  try {
    const { name, minPrice, maxPrice } = req.body;

    const searchCriteria = {};

    if (name && minPrice !== undefined && maxPrice !== undefined) {
      searchCriteria.$and = [
        { name: { $regex: new RegExp(name, 'i') } },
        { price: { $gte: minPrice, $lte: maxPrice } }
      ];
    } else if (name) {
      searchCriteria.name = { $regex: new RegExp(name, 'i') };
    } else if (minPrice !== undefined && maxPrice !== undefined) {
      searchCriteria.price = { $gte: minPrice, $lte: maxPrice };
    }

    const products = await Product.find(searchCriteria);

    if (products.length === 0) {
      return res.status(404).json({ message: 'No products found with the given criteria' });
    }

    res.status(200).json({ products });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};




const bcrypt = require('bcrypt');
const mongoose = require("mongoose")
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const auth = require("../auth");


module.exports.registerUser = (req, res) => {
	if(req.body.mobileNo.length != 11){
		return res.status(400).send({error: "Invalid Mobile Number"})
	} else if (!req.body.email.includes('@')){
		return res.status(400).send({error: "Invalid email format"})
	} else if (req.body.password.length < 8){
		return res.status(400).send({error: "Password should be at least 8 characters"})
	} else {

		User.findOne({email: req.body.email})
		.then(result =>{
			if(result){
				return res.status(409).send({error: 'Email already exists'})
			} else {

				let newUser = new User({
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					email: req.body.email,
					password: bcrypt.hashSync(req.body.password, 10),
					mobileNo: req.body.mobileNo
				})

				return newUser.save()
				.then((user) => res.status(201).send({ message: "Registered Successfully"}))
				.catch(err => {
					console.error("Error in registering", err)
					return res.status(500).send({error: "Failed to register"})

				})
			}
		})	

	}
}


module.exports.loginUser = (req, res) => {	

	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.status(404).send({ error: "No Email Found" });
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			if (isPasswordCorrect) {
				return res.status(200).send({ access : auth.createAccessToken(result)})
			} else {
				return res.status(401).send({ message: "Email and/or password do not match" });
			}

		}
	}).catch(err => res.status(500).send({error: "There is an error logging in"}))
}


module.exports.getDetails = (req, res) => {
	return User.findById(req.user.id)
	.then(user => {
	    if (!user) {
	        return res.status(404).send({ error: 'User not found' });
	    }
	    user.password = undefined;

	    return res.status(200).send({ user });
	})
	.catch(err => {
		console.error("Error in fetching user profile", err)
		return res.status(500).send({ error: 'Failed to fetch user profile' })
	});
};


module.exports.updateToAdmin = async (req, res) => {
  const id  = req.params.userId;
  console.log(id)
  try {
    // Check if the target user exists
    const targetUser = await User.findById(id);
    if (!targetUser) {
      	return res.status(404).send({ message: 'User not found' });
    } else if (targetUser.isAdmin == true){
    	return res.status(409).send({ message: 'User is already an Admin' });
    }

    targetUser.isAdmin = true;
    await targetUser.save();

    res.status(200).send({ message: 'User updated to admin successfully', targetUser});
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal Server Error' });
  }
};

// module.exports.demoteFromAdmin = async (req, res) => {
//   const id  = req.params.userId;
//   console.log(id)
//   try {
//     // Check if the target user exists
//     const targetUser = await User.findById(id);
//     if (!targetUser) {
//       	return res.status(404).send({ message: 'User not found' });
//     } else if (targetUser.isAdmin == false){
//     	return res.status(409).send({ message: 'User is not an Admin' });
//     }

//     targetUser.isAdmin = false;
//     await targetUser.save();

//     res.status(200).send({ message: 'User updated to admin successfully' });
//   } catch (error) {
//     console.error(error);
//     res.status(500).send({ message: 'Internal Server Error' });
//   }
// };


module.exports.updatePassword = async (req, res) => {
  try {
    const { newPassword } = req.body; 
    const { id } = req.user; 

    const hashedPassword = await bcrypt.hash(newPassword, 10);
    await User.findByIdAndUpdate(id, { password: hashedPassword });
    res.status(200).send({ message: 'Password updated' });

  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error' });
  }
};



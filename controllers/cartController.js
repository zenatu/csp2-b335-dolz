const bcrypt = require('bcrypt');
const mongoose = require("mongoose")
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const auth = require("../auth");
const Order = require('../models/order');

// -------------------------------------- CART---------------------------



// module.exports.addToCart = (req, res) => {
// 	const userId = req.user.id;
//   const productId = req.body.productId;
//   const quantity = req.body.quantity; 

//   const user = User.findById(userId);
//   if(!user){
//   	return res.status(404).json({ message: 'User not found' });
//   }

//   const product = Product.findById(productId);
//   if(!product){
//   	return res.status(404).json({ message: 'User not found' });
//   }

// }





// module.exports.addToCart = async (req, res) => {
//   const userId = req.user.id;  
//   const productId = req.body.productId;
//   const quantity = req.body.quantity; 


//   try {
//     // Find the user by ID
//     const user = await User.findById(userId);

//     if (!user) {
//       return res.status(404).json({ message: 'User not found' });
//     }

//     // Find the product by ID
//     const product = await Product.findById(productId);

//     if (!product) {
//       return res.status(404).json({ message: 'Product not found' });
//     }

//     // Find the user's cart or create a new one
//     let userCart = await Cart.findOne({ userId });

//     if (!userCart) {
//       userCart = new Cart({ userId, cartItems: [], totalPrice: 0 });
//     }

//     // Calculate the subtotal based on the product price
//     const subtotal = product.price * quantity;

//     // Add the item to the cart
//     userCart.cartItems.push({ productId, quantity, subtotal });

//     // Calculate total price
//     userCart.totalPrice += subtotal;

//     // Save the updated user cart
//     await userCart.save();

//     // Link the cart to the user
//     user.cart = userCart._id;
//     await user.save();

//     res.status(200).json({ message: 'Item added to cart successfully', cart: userCart });
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ message: 'Internal Server Error' });
//   }
// };

// fixed addtoCart pushing to the end of the index instead of adding to existing product
module.exports.addToCart = async (req, res) => {
  const userId = req.user.id;
  const productId = req.body.productId;
  const quantity = req.body.quantity;

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the product by ID
    // const product = await Product.findById(productId);
    const product = await Product.findOne({ _id: productId, isActive: true });

    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    // Find the user's cart or create a new one
    let userCart = await Cart.findOne({ userId });

    if (!userCart) {
      userCart = new Cart({ userId, cartItems: [], totalPrice: 0 });
    }

    // Check if the product is already in the user's cart
    const cartItemIndex = userCart.cartItems.findIndex(item => item.productId.toString() === productId.toString());

    if (cartItemIndex !== -1) {
      // If the product is already in the cart, update quantity and subtotal
      const existingCartItem = userCart.cartItems[cartItemIndex];
      existingCartItem.quantity += quantity;
      existingCartItem.subtotal += product.price * quantity;
    } else {
      // If the product is not in the cart, add a new cart item
      const subtotal = product.price * quantity;
      userCart.cartItems.push({ productId, quantity, subtotal });
    }

    // Calculate total price
    userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);

    // Save the updated user cart
    await userCart.save();

    // Link the cart to the user
    user.cart = userCart._id;
    await user.save();

    res.status(200).json({ message: 'Item added to cart successfully', cart: userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};


module.exports.getUserCart = async (req, res) => {
  const userId = req.user.id;  // Assuming you're using authentication middleware to get the user ID

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the user's cart
    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ message: 'Cart not found for the user' });
    }

    res.status(200).json({ cart: userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};


// Change Quantity
// REVIEW PART OF THIS CODE LATER
module.exports.changeQuantity = async (req, res) => {
  const userId = req.user.id;  // Assuming you're using authentication middleware to get the user ID
  const { productId, quantity } = req.body;

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the user's cart
    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ message: 'Cart not found for the user' });
    }

    // Find the cart item with the specified productId

    // previous code not working needed to refactor
    // const cartItem = userCart.cartItems.find(item => item.productId === productId);

    // Review this MAGIC CODE later
    const cartItemIndex = userCart.cartItems.findIndex(item => item.productId.toString() === productId.toString());

    if (cartItemIndex === -1) {
      return res.status(404).json({ message: 'Product not found in the cart' });
    }

    const { productId: foundProductId, quantity: oldQuantity, subtotal } = userCart.cartItems[cartItemIndex];

    // Update the quantity and subtotal based on the new quantity
    userCart.cartItems[cartItemIndex].quantity = quantity;
    userCart.cartItems[cartItemIndex].subtotal = (subtotal / oldQuantity) * quantity;

    // Update the total price in the cart
    userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);

    // Save the updated user cart
    await userCart.save();

    res.status(200).json({ message: 'Quantity updated successfully', cart: userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};



// remove Product from Cart
module.exports.removeProductFromCart = async (req, res) => {
  const userId = req.user.id;  // Assuming you're using authentication middleware to get the user ID
  const  productId  = req.params.productId;

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the user's cart
    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ message: 'Cart not found for the user' });
    }

    // Find the cart item with the specified productId
    const cartItemIndex = userCart.cartItems.findIndex(item => item.productId.toString() === productId.toString());

    if (cartItemIndex === -1) {
      return res.status(404).json({ message: 'Product not found in the cart' });
    }

    // Remove the cart item from the array
    const removedCartItem = userCart.cartItems.splice(cartItemIndex, 1)[0];

    // Update the total price in the cart
    userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);

    // Save the updated user cart
    await userCart.save();

    res.status(200).json({ message: 'Product removed from the cart', removedCartItem, cart: userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};


module.exports.clearCart = async (req, res) => {
  const userId = req.user.id;

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the user's cart
    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ message: 'Cart not found for the user' });
    }

    // Remove the entire cart instance
    await Cart.deleteOne({ userId });

    res.status(200).json({ message: 'Cart cleared successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

module.exports.checkout = async (req, res) => {
  try {
    const userId = req.user.id;
    const cart = await Cart.findOne({ userId });

    if (!cart || cart.cartItems.length === 0) {
      return res.status(400).json({ message: "Cart is empty" });
    }

    // Log the cart details for debugging
    console.log("Cart:", cart);

    // Convert the cart to an order
    const order = await cart.convertToOrder();

    // Log the order details for debugging
    console.log("Order:", order);

    res.status(201).json({ message: "Checkout successful", order });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};




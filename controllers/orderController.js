const bcrypt = require('bcrypt');
const mongoose = require("mongoose")
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const auth = require("../auth");
const Order = require('../models/order');


module.exports.getAllOrders = (req, res) => {
	return Order.find({})
	.then(orders => {
	    if(orders.length > 0){
	    	res.status(200).send({ orders });
	    } else {
	        return res.status(200).send({ message: 'No porders found.' });
	    }
	}).catch (err => res.status(500).send({ error: 'Error finding orders.'}))
};

// module.exports.getAllOrders = async (req, res) => {
//   try {
//     // Retrieve all orders
//     const orders = await Order.find().populate('userId', 'firstName lastName');

//     res.status(200).json({ orders });
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ message: 'Internal Server Error' });
//   }
// };

module.exports.getUserOrders = (req, res) => {
  const userId = req.user.id;

  Order.find({ userId })
    .then((orders) => {
      if (orders.length > 0) {
        res.status(200).send({ orders });
      } else {
        return res.status(200).send({ message: 'No orders found for the user.' });
      }
    })
    .catch((err) => res.status(500).send({ error: 'Error finding user orders.' }));
};

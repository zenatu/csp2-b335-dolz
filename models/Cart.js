const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  cartItems: [
    {
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Product',
          required: [true, 'productId is Required']
        },
        quantity: {
          type: Number,
          required: [true, 'quantity is Required']
        },
        subtotal: {
          type: Number,
          required: [true, 'subtotal is Required']
        }
    }
  ],
  totalPrice: {
    type: Number,
    required: [true, 'totalPrice is Required']
  },
  orderedOn: {
    type: Date,
    default: Date.now
  }
});



// cartSchema.methods.convertToOrder = async function () {
//   const Order = mongoose.model("Order");

//   // Create an order based on the cart
//   const order = new Order({
//     userId: this.userId,
//     productsOrdered: [...this.cartItems],
//     totalPrice: this.totalPrice,
//   });

//   // Clear the cart after creating the order
//   this.cartItems = [];
//   this.totalPrice = 0;
//   await this.save();

//   // Save the order
//   await order.save();

//   return order;
// };


// delete Cart after Order

cartSchema.methods.convertToOrder = async function () {
  const Order = mongoose.model("Order");

  try {
    // Create an order based on the cart
    const order = new Order({
      userId: this.userId,
      productsOrdered: [...this.cartItems],
      totalPrice: this.totalPrice,
      status: 'Paid',
    });

    // Save the order
    await order.save();

    // Delete the cart after creating the order
    await this.deleteOne();

    return order;
  } catch (error) {
    console.error("Error converting cart to order:", error);
    throw error; // Re-throw the error to handle it elsewhere if needed
  }
};





// Export the Cart model
module.exports = mongoose.model("Cart", cartSchema);



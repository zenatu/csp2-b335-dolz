const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please input the Product name"]
  },
  description: {
    type: String,
    required: [true, "Please input the Product description"]
  },
  price: {
    type: Number,
    required: [true, "Please input the Product price"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Product", productSchema);
